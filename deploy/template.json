{
    "$schema": "https://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
    "contentVersion": "1.0.0.0",
    "parameters": {
      "adminUser": {
        "metadata": {
          "description": "Username of the local admin user"
        },
        "type": "string",
        "defaultValue": "admin-user"
      },
      "adminPassword": {
        "metadata": {
          "description": "Password of the local admin user"
        },
        "type": "securestring"
      },
      "vmSetCount": {
        "metadata": {
          "description": "How many sets of VMs to build. This should equal the number of lab participants"
        },
        "type": "int"
      }
    },
    "variables": {
        "shortUniqueString": "[substring(uniquestring(resourceGroup().id), 0, 13)]",
        "storageaccountname": "[concat('dotnetdiag',variables('shortUniqueString'))]"
    },
    "resources": [
        {
            "type": "Microsoft.Network/networkSecurityGroups",
            "apiVersion": "2018-12-01",
            "name": "dotnet-nsg",
            "location": "[resourceGroup().location]",
            "properties": {
                "securityRules": [
                    {
                        "name": "Allow_All_From_ABC-Slalom",
                        "properties": {
                            "protocol": "*",
                            "sourcePortRange": "*",
                            "destinationPortRange": "*",
                            "destinationAddressPrefix": "*",
                            "access": "Allow",
                            "priority": 100,
                            "direction": "Inbound",
                            "sourceAddressPrefixes": [
                                "12.25.104.194",
                                "76.183.216.131",
                                "168.61.185.11",
                                "67.198.63.129",
                                "40.122.67.49",
                                "104.209.182.178",
                                "108.216.136.224",
                                "108.52.240.116",
                                "107.204.22.94",
                                "34.200.62.130",
                                "209.182.170.240/28"
                            ]
                        }
                    },
                    {
                        "name": "Allow_HTTP",
                        "properties": {
                            "protocol": "*",
                            "sourcePortRange": "*",
                            "destinationPortRange": "80",
                            "destinationAddressPrefix": "*",
                            "access": "Allow",
                            "priority": 200,
                            "direction": "Inbound",
                            "sourceAddressPrefix": "*"
 
                        }
                    }
                ]
            }
        },
        {
            "type": "Microsoft.Network/virtualNetworks",
            "apiVersion": "2018-12-01",
            "name": "dotnet-vnet",
            "location": "[resourceGroup().location]",
            "dependsOn": ["[resourceId('Microsoft.Network/networkSecurityGroups','dotnet-nsg')]"],
            "properties": {
                "addressSpace": {
                    "addressPrefixes": [
                        "172.30.0.0/16"
                    ]
                },
                "subnets": [
                    {
                        "name": "dotnet-subnet1",
                        "properties": {
                            "addressPrefix": "172.30.1.0/24",
                            "delegations": [],
                            "networkSecurityGroup": {
                                "id": "[resourceId('Microsoft.Network/networkSecurityGroups','dotnet-nsg')]"
                            }
                        }
                    }
                ],
                "virtualNetworkPeerings": [],
                "enableDdosProtection": false,
                "enableVmProtection": false
            }
        },
        {
            "type": "Microsoft.Storage/storageAccounts",
            "apiVersion": "2018-07-01",
            "name": "[variables('storageaccountname')]",
            "location": "[resourceGroup().location]",
            "sku": {
                "name": "Standard_LRS",
                "tier": "Standard"
            },
            "kind": "Storage",
            "properties": {
                "networkAcls": {
                    "bypass": "AzureServices",
                    "virtualNetworkRules": [],
                    "ipRules": [],
                    "defaultAction": "Allow"
                },
                "supportsHttpsTrafficOnly": false,
                "encryption": {
                    "services": {
                        "file": {
                            "enabled": true
                        },
                        "blob": {
                            "enabled": true
                        }
                    },
                    "keySource": "Microsoft.Storage"
                }
            }
        },
        {
            "type": "Microsoft.Network/publicIPAddresses",
            "apiVersion": "2018-12-01",
            "name": "[concat('sql', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": ["[resourceId('Microsoft.Network/virtualNetworks','dotnet-vnet')]"],
            "sku": {
                "name": "Basic",
                "tier": "Regional"
            },
            "copy": {
                "name": "sqlPipLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "publicIPAddressVersion": "IPv4",
                "publicIPAllocationMethod": "Static",
                "idleTimeoutInMinutes": 4,
                "ipTags": []
            }
        },
        {
            "type": "Microsoft.Network/publicIPAddresses",
            "apiVersion": "2018-12-01",
            "name": "[concat('iis', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": ["[resourceId('Microsoft.Network/virtualNetworks','dotnet-vnet')]"],
            "sku": {
                "name": "Basic",
                "tier": "Regional"
            },
            "copy": {
                "name": "iisPipLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "publicIPAddressVersion": "IPv4",
                "publicIPAllocationMethod": "Static",
                "idleTimeoutInMinutes": 4,
                "ipTags": []
            }
        },
        {
            "type": "Microsoft.Network/publicIPAddresses",
            "apiVersion": "2018-12-01",
            "name": "[concat('build', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": ["[resourceId('Microsoft.Network/virtualNetworks','dotnet-vnet')]"],
            "sku": {
                "name": "Basic",
                "tier": "Regional"
            },
            "copy": {
                "name": "buildPipLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "publicIPAddressVersion": "IPv4",
                "publicIPAllocationMethod": "Static",
                "idleTimeoutInMinutes": 4,
                "ipTags": []
            }
        },
        {
            "type": "Microsoft.Network/publicIPAddresses",
            "apiVersion": "2018-12-01",
            "name": "elasticsearch",
            "dependsOn": ["[resourceId('Microsoft.Network/virtualNetworks','dotnet-vnet')]"],
            "location": "[resourceGroup().location]",
            "sku": {
                "name": "Basic",
                "tier": "Regional"
            },
            "properties": {
                "publicIPAddressVersion": "IPv4",
                "publicIPAllocationMethod": "Static",
                "idleTimeoutInMinutes": 4,
                "ipTags": []
            }
        },
        {
            "type": "Microsoft.Network/networkInterfaces",
            "apiVersion": "2018-12-01",
            "name": "[concat('sql', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": [
                "sqlPipLoop",
                "[resourceId('Microsoft.Network/virtualNetworks','dotnet-vnet')]"
            ],
            "copy": {
                "name": "sqlNicLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "ipConfigurations": [
                    {
                        "name": "ipconfig1",
                        "properties": {
                            "publicIPAddress": {
                                "id": "[resourceId('Microsoft.Network/publicIPAddresses', concat('sql', padLeft(copyIndex(1), 2, '0')))]"
                            },
                            "subnet": {
                                "id": "[resourceId('Microsoft.Network/virtualNetworks/subnets', 'dotnet-vnet', 'dotnet-subnet1')]"
                            },
                            "primary": true,
                            "privateIPAddressVersion": "IPv4"
                        }
                    }
                ],
                "dnsSettings": {
                    "dnsServers": [],
                    "appliedDnsServers": []
                },
                "enableAcceleratedNetworking": false,
                "enableIPForwarding": false,
                "primary": true,
                "tapConfigurations": []
            }
        },
        {
            "type": "Microsoft.Network/networkInterfaces",
            "apiVersion": "2018-12-01",
            "name": "[concat('iis', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": [
                "iisPipLoop",
                "[resourceId('Microsoft.Network/virtualNetworks','dotnet-vnet')]"
            ],
            "copy": {
                "name": "iisNicLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "ipConfigurations": [
                    {
                        "name": "ipconfig1",
                        "properties": {
                            "publicIPAddress": {
                                "id": "[resourceId('Microsoft.Network/publicIPAddresses', concat('iis', padLeft(copyIndex(1), 2, '0')))]"
                            },
                            "subnet": {
                                "id": "[resourceId('Microsoft.Network/virtualNetworks/subnets', 'dotnet-vnet', 'dotnet-subnet1')]"
                            },
                            "primary": true,
                            "privateIPAddressVersion": "IPv4"
                        }
                    }
                ],
                "dnsSettings": {
                    "dnsServers": [],
                    "appliedDnsServers": []
                },
                "enableAcceleratedNetworking": false,
                "enableIPForwarding": false,
                "primary": true,
                "tapConfigurations": []
            }
        },
        {
            "type": "Microsoft.Network/networkInterfaces",
            "apiVersion": "2018-12-01",
            "name": "[concat('build', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": [
                "buildPipLoop",
                "[resourceId('Microsoft.Network/virtualNetworks','dotnet-vnet')]"
            ],
            "copy": {
                "name": "buildNicLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "ipConfigurations": [
                    {
                        "name": "ipconfig1",
                        "properties": {
                            "publicIPAddress": {
                                "id": "[resourceId('Microsoft.Network/publicIPAddresses', concat('build', padLeft(copyIndex(1), 2, '0')))]"
                            },
                            "subnet": {
                                "id": "[resourceId('Microsoft.Network/virtualNetworks/subnets', 'dotnet-vnet', 'dotnet-subnet1')]"
                            },
                            "primary": true,
                            "privateIPAddressVersion": "IPv4"
                        }
                    }
                ],
                "dnsSettings": {
                    "dnsServers": [],
                    "appliedDnsServers": []
                },
                "enableAcceleratedNetworking": false,
                "enableIPForwarding": false,
                "primary": true,
                "tapConfigurations": []
            }
        },
        {
            "type": "Microsoft.Network/networkInterfaces",
            "apiVersion": "2018-12-01",
            "name": "elasticsearch",
            "location": "[resourceGroup().location]",
            "dependsOn": [
                "[resourceId('Microsoft.Network/publicIPAddresses', 'elasticsearch')]",
                "[resourceId('Microsoft.Network/virtualNetworks','dotnet-vnet')]"
            ],
            "properties": {
                "ipConfigurations": [
                    {
                        "name": "ipconfig1",
                        "properties": {
                            "publicIPAddress": {
                                "id": "[resourceId('Microsoft.Network/publicIPAddresses', 'elasticsearch')]"
                            },
                            "subnet": {
                                "id": "[resourceId('Microsoft.Network/virtualNetworks/subnets', 'dotnet-vnet', 'dotnet-subnet1')]"
                            },
                            "primary": true,
                            "privateIPAddressVersion": "IPv4"
                        }
                    }
                ],
                "dnsSettings": {
                    "dnsServers": [],
                    "appliedDnsServers": []
                },
                "enableAcceleratedNetworking": false,
                "enableIPForwarding": false,
                "primary": true,
                "tapConfigurations": []
            }
        },
        {
            "type": "Microsoft.Compute/virtualMachines",
            "apiVersion": "2018-10-01",
            "name": "[concat('build', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": [
                "buildNicLoop",
                "[resourceId('Microsoft.Storage/StorageAccounts', variables('storageaccountname'))]"
            ],
            "copy": {
                "name": "buildVmLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "hardwareProfile": {
                    "vmSize": "Standard_D2s_v3"
                },
                "storageProfile": {
                    "imageReference": {
                        "id":"[resourceId('dev-test-lab','Microsoft.Compute/images/','ABC-CICD-Windows-Jenkins')]"
                    },
                    "osDisk": {
                        "osType": "Windows",
                        "name": "[concat(concat('build', padLeft(copyIndex(1), 2, '0')), '_OsDisk')]",
                        "createOption": "FromImage",
                        "caching": "ReadWrite",
                        "managedDisk": {
                            "storageAccountType": "Standard_LRS"
                        },
                        "diskSizeGB": 127
                    },
                    "dataDisks": []
                },
                "osProfile": {
                    "computerName": "[concat('build', padLeft(copyIndex(1), 2, '0'))]",
                    "adminUsername": "[parameters('adminUser')]",
                    "adminPassword": "[parameters('adminPassword')]",
                    "windowsConfiguration": {
                        "provisionVMAgent": true,
                        "enableAutomaticUpdates": true
                    },
                    "secrets": [],
                    "allowExtensionOperations": true
                },
                "networkProfile": {
                    "networkInterfaces": [
                        {
                            "id": "[resourceId('Microsoft.Network/networkInterfaces', concat('build', padLeft(copyIndex(1), 2, '0')))]"
                        }
                    ]
                },
                "diagnosticsProfile": {
                    "bootDiagnostics": {
                        "enabled": true,
                        "storageUri": "[concat('https://', variables('storageaccountname'), '.blob.core.windows.net/')]"
                    }
                }
            }
        },
        {
            "type": "Microsoft.Compute/virtualMachines",
            "apiVersion": "2018-10-01",
            "name": "[concat('sql', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": [
                "sqlNicLoop",
                "[resourceId('Microsoft.Storage/StorageAccounts', variables('storageaccountname'))]"
            ],
            "copy": {
                "name": "sqlVmLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "hardwareProfile": {
                    "vmSize": "Standard_D2s_v3"
                },
                "storageProfile": {
                    "imageReference": {
                        "id":"[resourceId('dev-test-lab','Microsoft.Compute/images/','ABC-CICD-Windows-SQL')]"
                    },
                    "osDisk": {
                        "osType": "Windows",
                        "name": "[concat(concat('sql', padLeft(copyIndex(1), 2, '0')), '_OsDisk')]",
                        "createOption": "FromImage",
                        "caching": "ReadWrite",
                        "managedDisk": {
                            "storageAccountType": "Standard_LRS"
                        },
                        "diskSizeGB": 127
                    },
                    "dataDisks": [
                            {
                            "lun": 0,
                            "name": "[concat(concat('sql', padLeft(copyIndex(1), 2, '0')), '_DataDisk01')]",
                            "createOption": "FromImage",
                            "caching": "ReadOnly",
                            "writeAcceleratorEnabled": false,
                            "managedDisk": {
                                "storageAccountType": "Premium_LRS"
                            },
                            "diskSizeGB": 1023
                        }
                    ]
                },
                "osProfile": {
                    "computerName": "[concat('sql', padLeft(copyIndex(1), 2, '0'))]",
                    "adminUsername": "[parameters('adminUser')]",
                    "adminPassword": "[parameters('adminPassword')]",
                    "windowsConfiguration": {
                        "provisionVMAgent": true,
                        "enableAutomaticUpdates": true
                    },
                    "secrets": [],
                    "allowExtensionOperations": true
                },
                "networkProfile": {
                    "networkInterfaces": [
                        {
                            "id": "[resourceId('Microsoft.Network/networkInterfaces', concat('sql', padLeft(copyIndex(1), 2, '0')))]"
                        }
                    ]
                },
                "diagnosticsProfile": {
                    "bootDiagnostics": {
                        "enabled": true,
                        "storageUri": "[concat('https://', variables('storageaccountname'), '.blob.core.windows.net/')]"
                    }
                }
            }
        },
        {
            "type": "Microsoft.Compute/virtualMachines",
            "apiVersion": "2018-10-01",
            "name": "[concat('iis', padLeft(copyIndex(1), 2, '0'))]",
            "location": "[resourceGroup().location]",
            "dependsOn": [
                "iisNicLoop",
                "[resourceId('Microsoft.Storage/StorageAccounts', variables('storageaccountname'))]"
            ],
            "copy": {
                "name": "iisVmLoop",
                "count": "[parameters('vmSetCount')]"
              },
            "properties": {
                "hardwareProfile": {
                    "vmSize": "Standard_D2s_v3"
                },
                "storageProfile": {
                    "imageReference": {
                        "id":"[resourceId('dev-test-lab','Microsoft.Compute/images/','ABC-CICD-Windows-IIS')]"
                    },
                    "osDisk": {
                        "osType": "Windows",
                        "name": "[concat(concat('iis', padLeft(copyIndex(1), 2, '0')), '_OsDisk')]",
                        "createOption": "FromImage",
                        "caching": "ReadWrite",
                        "managedDisk": {
                            "storageAccountType": "Standard_LRS"
                        },
                        "diskSizeGB": 127
                    },
                    "dataDisks": []
                },
                "osProfile": {
                    "computerName": "[concat('iis', padLeft(copyIndex(1), 2, '0'))]",
                    "adminUsername": "[parameters('adminUser')]",
                    "adminPassword": "[parameters('adminPassword')]",
                    "windowsConfiguration": {
                        "provisionVMAgent": true,
                        "enableAutomaticUpdates": true
                    },
                    "secrets": [],
                    "allowExtensionOperations": true
                },
                "networkProfile": {
                    "networkInterfaces": [
                        {
                            "id": "[resourceId('Microsoft.Network/networkInterfaces', concat('iis', padLeft(copyIndex(1), 2, '0')))]"
                        }
                    ]
                },
                "diagnosticsProfile": {
                    "bootDiagnostics": {
                        "enabled": true,
                        "storageUri": "[concat('https://', variables('storageaccountname'), '.blob.core.windows.net/')]"
                    }
                }
            }
        },
        {
            "type": "Microsoft.Compute/virtualMachines",
            "apiVersion": "2018-10-01",
            "name": "elasticsearch",
            "location": "[resourceGroup().location]",
            "dependsOn": [
                "[resourceId('Microsoft.Network/networkInterfaces', 'elasticsearch')]",
                "[resourceId('Microsoft.Storage/StorageAccounts', variables('storageaccountname'))]"
            ],
            "properties": {
                "hardwareProfile": {
                    "vmSize": "Standard_D2s_v3"
                },
                "storageProfile": {
                    "imageReference": {
                        "publisher": "Canonical",
                        "offer": "UbuntuServer",
                        "sku": "18.04-LTS",
                        "version": "latest"
                    },
                    "osDisk": {
                        "osType": "Linux",
                        "name": "[concat('elasticsearch', '_OsDisk')]",
                        "createOption": "FromImage",
                        "caching": "ReadWrite",
                        "managedDisk": {
                            "storageAccountType": "Standard_LRS"
                        },
                        "diskSizeGB": 30
                    },
                    "dataDisks": []
                },
                "osProfile": {
                    "computerName": "elasticsearch",
                    "adminUsername": "[parameters('adminUser')]",
                    "adminPassword": "[parameters('adminPassword')]",
                    "linuxConfiguration": {
                        "disablePasswordAuthentication": false,
                        "provisionVMAgent": true
                    },
                    "secrets": [],
                    "allowExtensionOperations": true
                },
                "networkProfile": {
                    "networkInterfaces": [
                        {
                            "id": "[resourceId('Microsoft.Network/networkInterfaces', 'elasticsearch')]"
                        }
                    ]
                },
                "diagnosticsProfile": {
                    "bootDiagnostics": {
                        "enabled": true,
                        "storageUri": "[concat('https://', variables('storageaccountname'), '.blob.core.windows.net/')]"
                    }
                }
            },
            "resources": [
                {
                    "type": "Microsoft.Compute/virtualMachines/extensions",
                    "apiVersion": "2018-10-01",
                    "name": "[concat('elasticsearch', '/CustomScriptForLinux')]",
                    "location": "[resourceGroup().location]",
                    "dependsOn": [
                        "[resourceId('Microsoft.Compute/virtualMachines', 'elasticsearch')]"
                    ],
                    "properties": {
                        "autoUpgradeMinorVersion": true,
                        "settings": {
                            "fileUris": [
                                "https://labsetupartifacts.blob.core.windows.net/scripts/install-docker.sh"
                            ]
                        },
                        "publisher": "Microsoft.OSTCExtensions",
                        "type": "CustomScriptForLinux",
                        "typeHandlerVersion": "1.4",
                        "protectedSettings": {
                            "commandToExecute": "sh install-docker.sh"
                        }
                    }
                }
            ]
        }
    ]
}